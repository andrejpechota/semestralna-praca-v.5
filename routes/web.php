<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('event/add', [EventController::class, 'create']);
Route::group(['middleware' => ['auth']], function () {
    Route::get('users', [UserController::class, 'index']);
    Route::resource('user', UserController::class);
    Route::get('user/{user}/delete', [UserController::class, 'destroy'])->name('user.delete');
    Route::resource('order', OrderController::class);
    Route::resource('ticket', TicketController::class);
    Route::get('event/{event}/edit', [UserController::class, 'edit']);
});
Route::get('get-more', [EventController::class, 'getMore']);
Route::get('find-users', [UserController::class, 'findUsers']);

Route::resource('event', EventController::class);

Route::resource('event', EventController::class);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

