@extends('layouts.root')
@section('homeImg')
    <img class="homeImg"
         src="https://lh3.googleusercontent.com/proxy/_qETy2S9v-a3XEkwKgv0iAJhOxWJkVXr5sWfFX7BJTCDhgl83PoENHmVGWO8CSl0ZNThgLkb20B6qfR_Hm4hMpEDmgenXgLydHbuyaV8XX-QgQSyr_tJOutPQZYGer7KsnFXpg"
         alt="obrazok koncert">
@endsection
@section('content')
    <div class="">
        <div class="container col-10">
            <div class="bg_home_notice">
                <h3>
                    Informácie k aktuálnej situácii
                </h3>
            </div>

            <div class="bg_home_notice mt-2 ">
                <p>
                    Vážení zákazníci,
                    na základe Verejnej vyhlášky, ktorú vydal <strong>ÚRAD VEREJNÉHO ZDRAVOTNÍCTVA SLOVENSKEJ
                        REPUBLIKY</strong> dňa <strong>9.3.2020</strong>, si Vás dovoľujeme informovať k aktuálnej
                    situácii
                    týkajúcej sa organizovania kultúrnych a športových podujatí:
                </p>
                <p>
                    <strong>1. Ak podujatie, na ktoré ste si kúpili vstupenky</strong>, je alebo bude z akéhokoľvek
                    dôvodu
                    <strong>zrušené</strong> (napr. všeobecným zákazom verejných podujatí zo strany príslušných orgánov
                    verejnej správy, alebo z dôvodov na strane organizátora alebo interpreta), <strong>budeme
                        Vás</strong> v
                    súlade s obchodnými podmienkami a pokynmi organizátora podujatia o tejto skutočnosti, ako aj o
                    prípadných možnostiach a náhradných termínoch informovať:
                    <strong>elektronickou poštou</strong> (ak máme pre tieto účely k dispozícií Vašu e-mailovú adresu)
                    alebo
                    prostredníctvom našej <strong>webovej stránky</strong>.
                </p>
                <p>
                    <strong>2. Ak podujatie, na ktoré ste si zakúpili vstupenky</strong>, nebolo k dnešnému dňu zrušené
                    vstupenky zostávajú i naďalej v platnosti a nevzniká dôvod na ich vrátenie.
                </p>
            </div>
        </div>
        <div class="homePageEventMenu">
            <a href="#tips" class="buttonEventMenu">Tipy</a>
            <a href="#today" class="buttonEventMenu">Dnes</a>
            <a href="#nextWeek" class="buttonEventMenu">Týždeň</a>
            <a href="#nextMonth" class="buttonEventMenu">Mesiac</a>
        </div>


        <div class="event_set" id="tips">

            <h2 class="with_line">
                Tips
                <span class="line"></span>
            </h2>
            <div class="homePageEvents">


                <div class="homePageEventsRow">

                    @forelse($eventsTips as $event)
                        <div class="eventHomePage">
                            <a href="/event/{{$event->id}}">
                                <img src="{{$event->imgUrl == '' ? URL::asset('img/noImg.png') : $event->imgUrl}}"
                                     alt="{{ $event->title }}"
                                     class="eventImgHomePage">
                            </a>
                            <div class="eventNameHP">
                                {{ $event->title }}
                            </div>
                            <div class="eventPriceHP">
                                Od {{ $event->price }} €
                            </div>


                        </div>
                    @empty
                        <p>Nič na zobrazenie</p>
                    @endforelse
                </div>
            </div>
        </div>

        <div class="event_set" id="today">
            <div class="with_line">
                <h2>Dnes/zajtra</h2>
                <div class="datesHomePage">{{ dateIntervalToStr(time(), strtotime('+1 day')) }}</div>
                <span class="line"></span>
            </div>
            @forelse($eventsToday as $event)
                <div class="event">
                    <a href="/event/{{$event->id}}">
                        <img src="{{$event->imgUrl == '' ? URL::asset('img/noImg.png') : $event->imgUrl}}"
                             alt="{{ $event->title }}"
                             class="eventImg">
                    </a>
                    <div class="eventNameHP">
                        {{ $event->title }}
                    </div>
                    <div class="eventPriceHP">
                        Od {{ $event->price }} €
                    </div>

                </div>
            @empty
                <p>Nič na zobrazenie</p>
            @endforelse
        </div>

        <div class="event_set" id="nextWeek">
            <div class="with_line">
                <h2>Následujúci týždeň</h2>
                <div class="datesHomePage">{{ dateIntervalToStr(time(), strtotime('+7 days')) }}</div>
                <span class="line"></span>
            </div>
            @forelse($eventsNextWeek as $event)
                <div class="event">
                    <a href="/event/{{$event->id}}">
                        <img src="{{$event->imgUrl == '' ? URL::asset('img/noImg.png') : $event->imgUrl}}"
                             alt="{{ $event->title }}"
                             class="eventImg">
                    </a>
                    <div class="eventNameHP">
                        {{ $event->title }}
                    </div>
                    <div class="eventPriceHP">
                        Od {{ $event->price }} €
                    </div>

                </div>
            @empty
                <p>Nič na zobrazenie</p>
            @endforelse
        </div>

        <div class="event_set" id="nextMonth">
            <div class="with_line">
                <h2>Následujúci mesiac</h2>
                <div class="datesHomePage">{{ dateIntervalToStr(time(), strtotime('+1 month')) }}</div>
                <span class="line"></span>
            </div>
            <div class="homePageEvents">
                @forelse($eventsNextMonth as $event)
                    <div class="event">
                        <a href="/event/{{$event->id}}">
                            <img src="{{$event->imgUrl == '' ? URL::asset('img/noImg.png') : $event->imgUrl}}"
                                 alt="{{ $event->title }}"
                                 class="eventImg">
                        </a>
                        <div class="eventNameHP">
                            {{ $event->title }}
                        </div>
                        <div class="eventPriceHP">
                            Od {{ $event->price }} €
                        </div>

                    </div>
                @empty
                    <p>Nič na zobrazenie</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection
