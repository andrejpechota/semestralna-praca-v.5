<div class="container">
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Meno</th>
            <th scope="col">Email</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @forelse($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <a href="{{ route('user.edit',[$user->id]) }}" title="Edit" class="btn btn-sm btn-primary">Edit</a>
                    <a href="{{ route('user.delete',[$user->id]) }}" title="Delete" data-method="DELETE"
                       class="btn btn-sm btn-danger" data-confirm="Are you sure?">Delete</a>
                </td>
            </tr>
        @empty
            <p>Nič na zobrazenie</p>
        @endforelse
        </tbody>
    </table>
</div>
