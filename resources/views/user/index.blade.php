@extends('layouts.root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Users') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="mb-3">
                            @can('create', \App\Models\User::class)
                                <div class="d-flex">
                                    <a href="{{route('user.create')}}" class="btn btn-success" role="button">Pridať
                                        užívateľa</a>
                                    <div class="search-container ml-auto">

                                        <input type="text" placeholder="Search.." name="search" id="searchBar">

                                    </div>
                                </div>
                            @endcan

                        </div>
                        <div id="users_table">
                            @include('user.user_table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
