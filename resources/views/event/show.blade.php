@extends('layouts.root')

@section('title', $event->title )
@section('content')
    <div class="event">
        <h1>
            {{ $event->title }}
        </h1>

        <div class="align_center">
            <img src="{{$event->imgUrl == '' ? URL::asset('img/noImg.png') : $event->imgUrl}}" class="eventImgAlone">
        </div>
        <div class="align_center">
            <div class="eventInfo">
                <h3 class="eventInfoItem col">Cena: {{ $event->price }}€</h3>
                <h3 class="eventInfoItem col"> {{ $event->town }} </h3>
                <h3 class="eventInfoItem col"> {{ $event->place }} </h3>
                <h3 class="eventInfoItem col"> {{ $event->dateTime }} </h3>
            </div>
        </div>

        <div class="eventLongDesc">
            <h2>Popis</h2>
            {{ $event->description }}
        </div>
    </div>


@endsection
