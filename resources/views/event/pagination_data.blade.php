<div class="container align_center page">
    @forelse($events as $event)
        <div class="event">
            <img src="{{$event->imgUrl == '' ? URL::asset('img/noImg.png') : $event->imgUrl}}" alt="obrazok podujatia" class="eventImg">

            <div class="eventName">
                {{ $event->title }}
            </div>
            <div class="eventShortDesc">
                {{ Str::limit($event->description, 100) }}
            </div>
            <div class="eventButton">
                <a href="{{  url('event', $event->id) }}">Viac info</a>
            </div>

            @auth
                @if(Auth::user()->email == 'admin@admin.sk')
                    <div class="eventEditDelete">

                        {!! Form::open([
                        'method' => 'GET',
                        'route' => ['event.edit', $event->id]
                    ]) !!}
                        {!! Form::submit('Upraviť') !!}
                        {!! Form::close() !!}

                        {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['event.destroy', $event->id]
                    ]) !!}
                        {!! Form::submit('Zmazať') !!}
                        {!! Form::close() !!}
                    </div>
                @endif
            @endauth
        </div>

    @empty
        <p>Nič na zobrazenie</p>
    @endforelse

</div>
{!! $events->links('pagination::bootstrap-4') !!}


