@extends('layouts.root')

@section('titleText', 'Pridávanie')
@section('content')
    <h1>@yield('actionText')</h1>
    <div class="form-group text-danger">
        @foreach($errors->all() as $error)
            {{ $error }}<br>
        @endforeach
    </div>

    <form method="post" action="{{ $action }}">
        @csrf
        @method($method)
        <div class="form-group">
            <label for="title">Názov podujatia</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Názov"
                   value="{{ old('title', @$event->title) }}">
        </div>

        <div class="form-group">
            <label for="category">Kategória</label>
            <select class="form-control" id="category" name="category" value="{{ old('category', @$event->category) }}">
                <option value="music">Hudba</option>
                <option value="culture">Kultúra</option>
                <option value="entertainment">Zábava</option>
                <option value="sport">Šport</option>
            </select>
        </div>

        <div class="form-group">
            <label for="description">Popis</label>
            <textarea type="text" class="form-control" id="description" name="description" rows="4"
                      placeholder="opis">{{ old('description', @$event->description) }}</textarea>
        </div>
        <div class="form-group">
            <label for="price">Cena</label>
            <input type="number" class="form-control" id="price" name="price" placeholder="59" step=".01"
                   value="{{ old('price', @$event->price) }}">
        </div>
        <div class="form-group">
            <label for="dateTime">Dátum</label>
            <input type="date" class="form-control" id="dateTime" name="dateTime" placeholder="15.05.2021"
                   value="{{ old('dateTime', @$event->dateTime) }}">
        </div>
        <div class="form-group">
            <label for="town">Mesto</label>
            <input type="text" class="form-control" id="town" name="town" placeholder="Bratislava"
                   value="{{ old('town', @$event->town) }}">
        </div>
        <div class="form-group">
            <label for="place">Miesto</label>
            <input type="text" class="form-control" id="place" name="place" placeholder="Zimný štadión Ondreja Nepelu"
                   value="{{ old('place', @$event->place) }}">
        </div>
        <div class="form-group">
            <label for="imgUrl">URL obrázku</label>
            <input type="text" class="form-control" id="imgUrl" name="imgUrl" placeholder="https::/www.imgexample.com/img12.png"
                   value="{{ old('imgUrl', @$event->imgUrl) }}">
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-primary form-control" value=@yield('submitButton')>
        </div>
    </form>

@endsection
