@extends('layouts.root')

@section('title', 'Aktuálne podujatia')
@section('content')

    @auth
        @if(Auth::user()->email == 'admin@admin.sk')
            <div class="popup">Podujatie {{ $pop }} bolo vymazané!</div>
        @endif
    @endauth

    <div id="events_data">
        @include('event.pagination_data')
    </div>

@endsection
