<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title') / Listkomat</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    {{--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
            integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/style.css')}}">

</head>

<?php ?>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-light">
        <a class="navbar-brand" href="/home"><img src="{{ URL::asset('img/logo.png') }}" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdownMenuLink"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Podujatia
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('event.index', ['cat' => 'all'])}}">Všetky</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('event.index', ['cat' => 'music']) }}">Hudba</a>
                        <a class="dropdown-item" href="{{ route('event.index', ['cat' => 'sport']) }}">Šport</a>
                        <a class="dropdown-item" href="{{ route('event.index', ['cat' => 'culture']) }}">Kultúra</a>
                        <a class="dropdown-item" href="{{ route('event.index', ['cat' => 'entertainment']) }}">Zábava</a>
                        @auth
                            @if(Auth::user()->email == 'admin@admin.sk')
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/event/add">Pridanie</a>
                            @endif
                        @endauth
                    </div>
                </li>


                <li class="nav-item">
                    <a class="nav-link ml-3" href="?a=kontakt">Kontakt</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @auth
                                @if(Auth::user()->email == 'admin@admin.sk')
                                    <a class="dropdown-item" href="/users">
                                        {{ __('Užívatelia') }}
                                    </a>
                                @else
                                    <a class="dropdown-item"
                                       href={{ route('order.index', ['user' => Auth::user()->id]) }}>
                                        {{ __('Moje objednávky') }}
                                    </a>
                                    <a class="dropdown-item"
                                       href={{ route('user.show', ['user' => Auth::user()->id]) }}>
                                        {{ __('Profil') }}
                                    </a>
                                @endif
                            @endauth
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>

        </div>
    </nav>
</header>

<div class="container-fluid p-0">
    <div class="container">
        <div class="homeImgBg">
            @yield('homeImg')
        </div>
    </div>
</div>

<div class="container mt-5">
    @yield('content')
</div>

<script src="{{ URL::asset('js/script.js') }}"></script>
</body>
</html>
