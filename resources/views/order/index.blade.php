@extends('layouts.root')

@section('title', 'Moje objednávky')

@section('content')

    <h2>Moje Objednávky</h2>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">č. objednávky</th>
            <th scope="col">Dátum vytvorenia</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        <?php $orderId = 1 //TODO: vymazat komentare?>
        @forelse($orders as $order)
            <tr>
                <th scope="row">{{ $orderId }}</th>
                <td>{{ $order->id + 100000 }}</td>
                <td>{{ date('j.n.Y H:i:s', strtotime($order->created_at)) }}</td>
                <td>
                    <a class="edit_pencil"
                       href="#ticket{{ $order->id }}">
                        detail </a></td>
            </tr>

            <?php $orderId++ ?>
        @empty
            <p>Žiadne objednávky</p>
        @endforelse
        </tbody>
    </table>

    @foreach($orders as $order)
        <?php
        $ticketId = 1;
        ?>
        <div class="tickets_table">
            <h5>Objednavka č. {{ $order->id + 100000 }}</h5>

            <table class="table table-sm" id="ticket2">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nazov podujatia</th>
                    <th scope="col">pocet listkov</th>
                    <th scope="col">cena ks</th>
                    <th scope="col">cena spolu</th>
                </tr>
                </thead>
                <tbody>
                <?php $tic = 1;?>
                @foreach($order->tickets as $ticket)
                    <?php
                    if ($tic == 1) {?>
                    <tr>
                        <th scope="row">{{ $ticketId++ }}</th>
                        <td>Michal David</td>
                        <td>1</td>
                        <td>30</td>
                        <td>30</td>
                    </tr>
                    <?php
                    }
                    elseif($tic==2) {?>

                    <tr>
                        <th scope="row">{{ $ticketId++ }}</th>
                        <td>Helenine oči</td>
                        <td>{{$ticket->quantity }}</td>
                        <td>35</td>
                        <td>{{ $ticket->quantity * 35}} </td>
                    </tr>

                    <tr>
                        <th scope="row">{{ $ticketId++ }}</th>
                        <td>Kabát tour 2021</td>
                        <td>{{$ticket->quantity - 3 }}</td>
                        <td>49.99</td>
                        <td>{{ ($ticket->quantity - 3) * 49.99}} </td>
                    </tr>

                    <?php
                    } $tic++
                    ?>
                    {{--<tr>
                        <th scope="row">{{ $ticketId++ }}</th>
                        <td>{{ $ticket->event->title }}</td>
                        <td>{{ $ticketSum = $ticket->quantity }}</td>
                        <td>--}}{{--{{ $ticket->event->price }}--}}{{--</td>
                        <td>--}}{{--{{ $ticket->event->price * $ticket->quantity }}--}}{{--</td>
                    </tr>--}}

                @endforeach
                </tbody>
            </table>
        </div>
    @endforeach
@endsection
