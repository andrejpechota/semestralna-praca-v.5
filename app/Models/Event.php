<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    public $timestamps = false;

    private $id;
    protected $title;
    protected $category;
    protected $description;
    protected $price;
    protected $dateTime;
    protected $town;
    protected $place;
    protected $imgUrl;

    protected $fillable = ['title', 'category', 'description', 'price', 'num_of_tickets', 'dateTime', 'town', 'place', 'imgUrl'];

    /*public function __construct()
    {
    }*/
}
