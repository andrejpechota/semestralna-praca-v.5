<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $eventsTips = Event::all()->where('dateTime', '<=' ,date("Y-m-d"));
        $eventsToday = Event::all()->where('dateTime', '<=', date('Y-m-d',strtotime('+1 day')));
        $eventsToday = $eventsToday->where('dateTime', '>=', date('Y-m-d'));

        $eventsNextWeek = Event::all()->where('dateTime', '<=', date('Y-m-d',strtotime('+7 days')));
        $eventsNextWeek = $eventsNextWeek->where('dateTime', '>=', date('Y-m-d'));

        $eventsNextMonth = Event::all()->where('dateTime', '<=', date('Y-m-d',strtotime('+1 month')));
        $eventsNextMonth = $eventsNextMonth->where('dateTime', '>=', date('Y-m-d'));
        return view('home', ['eventsTips' => $eventsTips, 'eventsToday' => $eventsToday, 'eventsNextWeek' => $eventsNextWeek, 'eventsNextMonth' => $eventsNextMonth]);
    }
}
