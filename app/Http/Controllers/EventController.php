<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MongoDB\Driver\Session;
use Symfony\Component\Console\Input\Input;

class EventController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request/*$search*/)
    {
        $pop = \session('popupText');
        $cat = $request->cat;

        if ($cat == 'all') {
            $events = Event::paginate(6);
            return view('event.index')->with('events', $events)->with('pop', $pop);
        }
        else {
            $events = Event::where('category', '=', $cat )->paginate(6);
            return view('event.index')->with('events', $events)->with('pop', $pop);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function create()
    {
        return view('event.add', [
            'action' => route('event.store'),
            'method' => 'post',
            'activeEvent' => '',
            'activeAdd' => 'active'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|min:3',
            'description' => 'required|min:50',
            'price' => 'required|numeric',
            'dateTime' => 'required|date',
            'town' => 'required|min:3',
            'place' => 'required|min:3'
        ]);
        $event = Event::create($request->all());

        $event->save();
        return redirect()->route('event.index', ['cat' => 'all']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        return view('event.show', ['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        return view('event.edit', [
            'event' => $event,
            'method' => 'put',
            'action' => route('event.update', $event->id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|min:3',
            'description' => 'required|min:50',
            'price' => 'required|numeric',
            'dateTime' => 'required|date',
            'town' => 'required|min:3',
            'place' => 'required|min:3'
        ]);
        $event = Event::findOrFail($id);
        $event->update($request->all());
        return redirect()->route('event.index', ['cat' => 'all']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy(Event $event)
    {
        $eventName = $event->title;
        $event->delete();

        return redirect('/event?cat=all')->with('popupText', $eventName);
    }

    function getMore(Request $request)
    {
        $page = $_GET['page'];
        if($request->ajax())
        {
            for ($i = 0; $i < $page; $i++) {
                $events = Event::paginate(6);
            }
            return view('event.pagination_data',compact('events'))->render();
        }
    }
}
