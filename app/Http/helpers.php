<?php

function dateIntervalToStr($dateFrom, $dateTo) {
    $monthFrom = '';
    if (date('M', $dateFrom) !== date('M', $dateTo)) {
        $monthFrom = date('M', $dateFrom);
    }
    $str = '(' . date('j', $dateFrom) . '. ' . $monthFrom . ' - ' . date('j', $dateTo) . '. ' .
        date('M', $dateTo) . ' ' . date('Y', $dateFrom) . ')';
    return $str;
}
