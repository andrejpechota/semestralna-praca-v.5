//oznámenie o vymazaní podujatia
var pop = $('.popup');
console.log(pop.text());

if (pop.text() !== 'Podujatie  bolo vymazané!') {
    pop.show();
    pop.fadeOut(5000);
}

// scroll na domovskej stranke
$('.homePageEventMenu').on('click', 'a', function () {
    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
})


// zobrazenie listkov k objednavke
var ticketTables = $('.tickets_table');
ticketTables.hide();
$('.edit_pencil').on('click', function () {
    var str = $(this).attr('href');
    str = str.slice(7);
    ticketTables.hide().eq(parseInt(str) - 1).show();
})


//ajax na vyhladavanie uzivatelov
$(document).ready(function () {
    $(document).on('keyup', '#searchBar', function (event) {
        event.preventDefault();
        var text = ($('#searchBar')[0].value);
        find(text);
    });
});

function find(text) {
    $.ajax({
        type: 'GET',
        url: 'find-users',
        data: {searchText: text},
        success: function (data) {
            $('#users_table').html(data);
        }

    });
}

//ajax strankovanie
$(document).ready(function () {
    $(document).on('click', '.pagination a', function (event) {
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        getMore(page);
        $('#events_data').fadeOut(200);
    });
});

function getMore(page) {
    $.ajax({
        type: 'GET',
        url: 'get-more',
        data: {page: page},
        success: function (data) {
            var newPage = $(data).find('.page');
            $('#events_data').html(data).fadeIn(300);

        }

    });
}
